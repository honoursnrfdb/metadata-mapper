# Honours Project #
### Metadata mapper ###

Automatic and manual metadata mapper that takes in a csv file and tries to automatically map the fields to specific DSpace Dublin Core fields.


The idea for the development of this tool arose from a request by the National Research Foundation of South Africa to migrate data into a DSpace repository. We identified that there was a need for an easy to use means to import data and map it to the appropriate metadata fields as used by DSpace. The tool developed and discussed in this paper aims to facilitate the deposit and migration of data into a DSpace repository. Machine learning is used to attempt to determine the appropriate Dublin Core metadata field to which each field of the input data belongs.

**See 'Downloads' for final report.**