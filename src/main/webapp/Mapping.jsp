<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page isELIgnored="false" %>
<%--
  Author: Craig Feldman
  Date created: 2015-07-21

  Mapping
--%>

<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Ingestion Manager - Submit</title>

    <base href="${pageContext.request.contextPath}">

    <!-- Bootstrap core CSS -->
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- Bootstrap theme -->
    <link href="bootstrap/css/bootstrap-theme.min.css" rel="stylesheet">

    <link href="css/style.css" rel="stylesheet">


</head>
<body role="document" onbeforeunload='setDefaultDropDown();'>
<!-- Fixed navbar -->
<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
                    aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/ingest">Ingestion Manager</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
                <li><a href="/ingest"><span class="glyphicon glyphicon-home"></span> Home</a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li>
                    <%
                        if (session.getAttribute("user_email") != null) {
                    %>
                    <a><span class="glyphicon glyphicon-user"></span> Logged&nbsp;in&nbsp;as: ${user_email}</a>
                    <%
                    } else {
                    %>
                    <a><span class="glyphicon glyphicon-user"></span> Not&nbsp;logged&nbsp;in</a>
                    <%
                        }
                    %>
                </li>
                <li>
                    <a href="/ingest/logout"><span class="glyphicon glyphicon-log-out"></span> Logout</a>
                </li>
            </ul>
        </div>
    </div>
</nav>

<div class="container">
    <%
        // TODO
        // if (session.getAttribute("user_email") != null && (Boolean) session.getAttribute("user")) {
        if (true) {
    %>
    <!-- Main Submission Content -->
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h3 class="panel-title">Review and correct mapping</h3>
        </div>
        <div class="panel-body">
            <form class="form-horizontal" id="form-mapping" name="form-mapping"
                  action="${pageContext.request.contextPath}/mapping-generator"
                  method="post" enctype="multipart/form-data">

                <p class="help-block">Please review and correct the metadata mappings as specified below. Simply choose
                    the main Dublin Core (DC) metadata field that the data belongs to. The secondary DC field is
                    optional.</p>

                <p class="help-block">By hovering your mouse over the <span class="glyphicon glyphicon-list" aria-hidden="true"></span>
                    icon, you can view some example data for that field.<br>The percentage indicates how many entries from
                    the given field were classified as that DC field.</p>

                <table class="table table-bordered" style="table-layout:fixed">
                    <thead>
                    <tr>
                        <th class="text-center">Field names
                        <span class="glyphicon glyphicon-question-sign" aria-hidden="true"
                              data-toggle="tooltip" data-container="body"
                              title="The field names or indexes."></span>
                        </th>
                        <th class="text-center">Primary DC field
                            <span class="glyphicon glyphicon-question-sign" aria-hidden="true"
                                  data-toggle="tooltip" data-container="body"
                                  title="The main Dublin Core field."></span>
                        </th>
                        <th class="text-center">Secondary DC field
                            <span class="glyphicon glyphicon-question-sign" aria-hidden="true"
                                  data-toggle="tooltip" data-container="body"
                                  title="The secondary Dublin Core field (optional)."></span>
                        </th>
                    </tr>
                    </thead>
                    <!-- Populate table dynamically -->
                    <tbody>
                    <c:forEach begin="0" end="${numFields - 1}" varStatus="loop">
                        <tr>
                            <!-- The title of the field, along with example data popover -->
                            <td style="vertical-align: middle;">
                                <!-- when user hovers over the glyph, they get a list of example data -->
                                <span class="glyphicon glyphicon-list" aria-hidden="true"
                                      style="font-size:20px; margin-right: 10px;"
                                      data-toggle="popover" data-container="body" title="Example data"
                                      data-content="${sampleContent[loop.index]}"></span>

                                <b>${headerFields[loop.index]}</b>
                            </td>
                            <!-- The main DC class -->
                            <td>
                                <select class="form-control" name="DC-primary" id="DC-primary-${loop.index}"
                                        onchange="updateSecondaryDCField(${loop.index})">
                                    <c:forEach items="${mappingResults[loop.index]}" var="result">
                                        <option value="${result.key}">
                                            [<c:out value="${result.value}"/>%]
                                            <c:out value="${result.key}"/>
                                        </option>
                                    </c:forEach>
                                </select>
                            </td>
                            <td>
                                <select class="form-control" name="DC-secondary" id="DC-secondary-${loop.index}">
                                </select>
                            </td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>


                <div class="form-group">
                    <div class="col-md-12">
                        <button type="button" class="btn btn-default pull-left" onclick="goBack()">Go Back</button>
                        <input type="submit" class="btn btn-primary pull-right" name="submit" value="Submit"
                               tabindex="3"/>
                    </div>

                </div>
            </form>
        </div>
    </div>
    <%
    } else {
    %>
    <!-- User is not logged in -->
    <p>You are not logged in. Please login to submit.</p>
    <a href="/ingest">
        <button type="button" class="btn btn-default"><span class="glyphicon glyphicon-home"></span> Go home</button>
    </a>
    <%
        }
    %>

</div>

<script src="bootstrap/js/jquery-1.11.3.min.js"></script>
<script src="bootstrap/js/bootstrap.min.js"></script>
<script src="bootstrap/js/docs.min.js"></script>

<script type="text/javascript" language="javascript">

    // executes these commands when page has loaded
    $(document).ready(function () {
        // enable popovers
        $('[data-toggle="popover"]').popover({html: true, trigger: "hover"});
        // enable tooltips
        $('[data-toggle="tooltip"]').tooltip({container: "body"});
        // dispay secondary DC fields based on DC-primary
        updateAllSecondaryDCFields();

    });

    function goBack() {
        window.history.back();
    }


    function setDefaultDropDown() {
        document.getElementById("select-collection").selectedIndex = -1;
    }

    /** Updates all the secondary DC field dropdowns */
    function updateAllSecondaryDCFields() {
        for (var i = 0; i < ${numFields}; ++i)
            updateSecondaryDCField(i);
    }

    // Maps DC-primary to DC-secondary
    var secondaryDCFields = ${secondaryDCFields};

    /**
     * Updates a DC-secondary select. Used when DC-primary changes
     * @param rowId the row that needs to be updated
     */
    function updateSecondaryDCField(rowId) {
        var primaryDCValue = document.getElementById("DC-primary-" + rowId).value;
        var secondary = document.getElementById("DC-secondary-" + rowId);

        secondary.options.length = secondaryDCFields[primaryDCValue].length;

        // iterate over the array that contains DC-secondary for this primaryDCValue
        for (var index in secondaryDCFields[primaryDCValue])
            secondary.options[index] = new Option(secondaryDCFields[primaryDCValue][index]);

        // first add the 'null' option
        secondary.add(new Option("[none]", ""), 0);
        secondary.selectedIndex = 0;

    }
</script>


</body>
</html>