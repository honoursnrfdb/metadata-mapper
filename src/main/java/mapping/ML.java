package mapping;

import services.Data;
import weka.classifiers.Classifier;
import weka.core.Attribute;
import weka.core.DenseInstance;
import weka.core.Instance;
import weka.core.Instances;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Craig Feldman
 *
 * Date created: 2015-09-17.
 *
 * A 'static' class used to perform machine learning classification on a string
 */
public class ML {
	private static Classifier classifier = null;
	private static Instances instances = null;

	static {
		try {
			// Get the DC field names
			ArrayList<String> classVal = new ArrayList<>(Data.getDCFieldNames());

			ArrayList<Attribute> attributeList = new ArrayList<>(Data.getAttributeList());

			instances = new Instances("dcmapper", attributeList, 0);
			instances.setClassIndex(instances.numAttributes() - 1);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Loads the classification/model file into the classifier.
	 * @param modelInputStream the model file
	 */
	public static void setClassifierFile(InputStream modelInputStream) {
		try {
			classifier = (Classifier) weka.core.SerializationHelper.read(modelInputStream);
		} catch (Exception e) {
			System.err.println("There was an error loading the ML model file.");
			e.printStackTrace();
		}
	}

	/**
	 * Classifies a given feature vector into a DC field
	 * @param features - the feature vector
	 * @return the DC-primary field
	 */
	public static String classify(List<Double> features) {
		double result = -1;

		double[] target = new double[features.size()];
		for (int i = 0; i < features.size(); i++) {
			target[i] = features.get(i);
		}

		Instance inst = new DenseInstance(1.0, target);
		inst.setDataset(instances);

		// TODO remove
		System.out.println(inst.toString());

		try {
			result = classifier.classifyInstance(inst);
		} catch (Exception e) {
			e.printStackTrace();
		}

		// TODO remoce
		System.out.println("Result: " + result);
		System.out.println(result + " -> " + inst.classAttribute().value((int) result));

		return inst.classAttribute().value((int) result);
	}
}
