package mapping;

import com.opencsv.CSVReader;
import services.Data;
import services.FeatureVector;
import services.StringFormatting;
import services.ValueThenKeyComparator;

import java.io.IOException;
import java.util.*;

import static org.apache.commons.lang3.StringEscapeUtils.escapeHtml4;

/**
 * @author Craig Feldman
 * Date created: 2015-07-21.
 *
 * The 'main' class responsible for performing the mapping
 */
public class MetadataMap {

	// Stores the DC field names from training file
	private String[] header = null;
	// Store a Feature Vector for each column of data
	private FeatureVector[] features = null;
	// Tracks each field's mapping info
	private List<Map<String, Integer>> tracker = null;
	// Keeps track of how many entries there are in each field (ignores blanks entries)
	private int[] entryCounter;
	// Stores some sample content for each field
	private List<List<String>> sampleContent;
	// The number of fields (columns) in the input data
	private int numFields = 0;

	private void debugFeatures() {
		System.out.printf("There are %d feature vectors.\n", numFields);
		for (int i = 0; i < numFields; ++i)
			System.out.println(features[i].toString());

	}

	public void debugTracker(){
		for (int i = 0; i < numFields; ++i){
			System.out.println(tracker.get(i).toString());
		}
	}

	/**
	 * Parses the input file and classifies each entry.
	 * Once classified, it updates a map for that field to update the count for that DC field
     * @param reader A CSVReader object
     *               @see CSVReader
	 * @param hasHeader true if the first line of the CSV is a header, false otherwise
	 */
	public void parseCSV(CSVReader reader, boolean hasHeader) {

		String[] nextLine = null;
		FeatureVector fv;
		int lineNumber = 0;

		boolean hasRun = false;

		// Process each line of the file
		while (true) {
			++lineNumber;
			try {
				if((nextLine = reader.readNext()) == null)
					break;
			} catch (IOException e) {
				System.err.printf("Error processing line %d\n", lineNumber);
				e.printStackTrace();
			}

			// Do some initialising if this is the first iteration.
			if (!hasRun) {
				numFields = nextLine.length;
				initializeTracker();
				initializeEntryCounter();
				initializeSampleContent();

				if (hasHeader) {
					header = nextLine;
					//TODO remove
					for (int i = 0; i < nextLine.length; i++) {
						String heading = nextLine[i];
						if (heading.trim().isEmpty())
							nextLine[i] = "Field " + (i + 1);

						// sanitise
						header[i] = escapeHtml4(header[i]);
						System.out.println("Heading: " + heading);
					}

					hasRun = true;
					continue;
				} else { // no header
					header = new String[numFields];
					for (int i = 0; i < numFields; ++i)
						header[i] = "Field " + (i + 1);
				}
				hasRun = true;
			}

			// Classify each column in this row
			for (int fieldIndex = 0; fieldIndex < numFields; ++fieldIndex) {
				fv = new FeatureVector();
				// get the entry and trim whitespace
				String currentData = nextLine[fieldIndex].trim();
				//TODO
				// If there is a blank entry dont try classify it
				if (currentData.isEmpty())
					continue;

				// increase the counter for the number of entries for this field
				++entryCounter[fieldIndex];

				fv.generateFeatures(currentData);

				System.out.println(currentData);
				System.out.println(fv + "\n");

				// Get the DC field classification
				String classification = ML.classify(fv.getFeatures());

				if (classification != null) {
					System.out.println(classification);
					Map<String, Integer> map = tracker.get(fieldIndex);
					// Increments map counter value by one
					map.merge(classification, 1, (oldValue, one) -> oldValue + one);
				}

				// Add to sample content
				addSampleContent(fieldIndex, currentData);
			}
		}
    }

	/**
	 * Adds sample data to a list for each field.
	 * Restricts the data size to 20 characters.
	 *
	 * @param fieldIndex the field which this data belongs to.
	 * @param data the sample data to add.   	 *
	 */
	private void addSampleContent(int fieldIndex, String data) {
		// sanitises the data for HTML (e.g. remove quote characters)
		data = escapeHtml4(data);
		sampleContent.get(fieldIndex).add(data);
	}

	/* Creates the array to count how many entries there are in each field */
	private void initializeEntryCounter() {
		entryCounter = new int[numFields];
		for (int i = 0; i < numFields; i++)
			entryCounter[i] = 0;
	}

	/** Creates a list where each index contains a map that tracks the score for that indexes field */
	private void initializeTracker() {
		List<String> DCFields = new ArrayList<>(Data.getDCFieldNames());
		tracker = new ArrayList<>();

		for (int i = 0; i < numFields; ++i){
			tracker.add(new HashMap<>());
			for (int j = 0; j < DCFields.size(); ++j)
				tracker.get(i).put(DCFields.get(j), 0);
		}
	}

	/** Creates a list where each index contains a list containing sample data for the field at that index */
	private void initializeSampleContent() {
		sampleContent = new ArrayList<>();
		for (int i = 0; i < numFields; ++i) {
			sampleContent.add(new ArrayList<>());
		}
	}

	/** Sorts the tracker map by value (score) then key */
	public List<List<Map.Entry<String, Integer>>> getSortedTracker(){
		List<List<Map.Entry<String, Integer>>> outerList = new ArrayList(new ArrayList<Map.Entry<String, Integer>>());

		for (int fieldIndex = 0; fieldIndex < numFields; ++fieldIndex){
			List<Map.Entry<String, Integer>> innerList = new ArrayList<>(tracker.get(fieldIndex).entrySet());
			Collections.sort(innerList, new ValueThenKeyComparator<>());
			outerList.add(innerList);
		}
		return outerList;
	}

	/**
	 * Converts the score for each DC-primary to a percentage that indicates the percent that
	 * matched that DC-primary field.
	 */
	public void convertScoresToPercentage() {
		int i = 0;
		for (Map<String, Integer> map : tracker) {
			double fieldNumEntries = entryCounter[i++];
			map.replaceAll((k, v) -> (int)((v / fieldNumEntries) * 100));
		}
	}

    /** @return an array containing the field titles */
    public String[] getHeader() { return header; }

	/** @return the number of fields within the CSV input file */
	public int getNumFields() { return numFields; }

	/** @return HTML formatted sampleContent list for popover */
	public List<String> getFormattedSampleContent() {
		List<String> toReturn = new LinkedList<>();
		for (int i = 0; i < numFields; ++i) {
			List<String> fieldList = sampleContent.get(i);
			StringBuilder sb = new StringBuilder("<ul>");
			fieldList.stream().forEach((String s) -> {
				sb.append("<li>");
				sb.append(StringFormatting.getFormatted(s, 40));
				sb.append("</li>");
			});
			sb.append("</ul>");
			toReturn.add(sb.toString());
		}
		return toReturn;
	}

	/* @return a list that contains a map for each DC field score. The list indices correspond to the field indices. */
	public List<Map<String, Integer>> getResults() { return tracker;}

}
