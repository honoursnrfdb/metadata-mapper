package services;

import java.util.LinkedList;
import java.util.List;

/**
 * @author Craig Feldman
 * Date created: 10-09-15.
 *
 * Used to store and calculate features of a string of data
 */
public class FeatureVector {

	// Stores the features to be returned
	List<Double> features;
	private int length;
	private int numWords;
	private int numMonthNames;
	private int numPersonNames;
	private int numDigits;
	private int numLetters;
	private int numCapitals;

	/** Creates a new feature vector object and sets to default values */
	public FeatureVector(){
		features = new LinkedList<>();
		resetValues();
	}

	/** Resets the values and clears the feature vector list */
	private void resetValues() {
		length = 0;
		numDigits = 0;
		numLetters = 0;
		numCapitals = 0;
		numWords = 0;
		numMonthNames = 0;
		numPersonNames = 0;

		features.clear();
	}

	/**
	 * Generates the features for the provided data string
	 * @param data the string that will have its features extracted
	 */
	public void generateFeatures(String data) {
		resetValues();
		if (data.isEmpty())
			return;

		length = data.length();
		//totalLength += length;

		for (int i = 0; i < length; ++i) {
			char c = data.charAt(i);

			if (Character.isDigit(c))
				++numDigits;

			else if (Character.isLetter(c)) {
				++numLetters;
				// count capitals
				if (Character.isUpperCase(c))
					++numCapitals;
			}
		}

		String[] parts = data.split(" ");

		// Get the number of words
		numWords = parts.length;


		for (String part : parts) {
			// Regex used to remove non unicode characters (e.g. 14/Sep/2015 -> Sep -> sep)
			String alphaPart = part.replaceAll("\\P{L}+", "").toLowerCase();
			// Need to also prevent a blank string from matching
			if (alphaPart.length() == 0)
				continue;

			if (Data.getMonthSet().contains(alphaPart))
				++numMonthNames;

				// People may have a name like 'August', but it is more likely that that is a month name and hence 'else if'
			else if (Data.getNameSet().contains(alphaPart))
				++numPersonNames;
		}

		generateFinalFeatures();
	}

	/** Generates final features such as percentages */
	private void generateFinalFeatures() {
		double dLength = (double) length;
		//double dNumEntries = (double) numEntries;
		//double avgLength = totalLength / dNumEntries;
		//features.add(fieldName);

		// num characters
		features.add((double) length);
		// num words
		features.add((double) numWords);
		// num month names
		features.add((double) numMonthNames);
		// num person names
		features.add((double) numPersonNames);
		// % digits
		features.add(numDigits / dLength);
		// % letters
		features.add(numLetters / dLength);
		// % capitals
		features.add(numCapitals / dLength);

	}

	@Override
	public String toString() {
		return "FeatureVector{" +
				"length=" + length +
				", numWords=" + numWords +
				", numMonthNames=" + numMonthNames +
				", numPersonNames=" + numPersonNames +
				", numDigits=" + numDigits +
				", numLetters=" + numLetters +
				", numCapitals=" + numCapitals +
				", features=" + features +
				'}';
	}

	/** @return	The feature vector for the String */
	public List<Double> getFeatures() {
		return features;
	}
}
