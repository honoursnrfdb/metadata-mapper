package services;

import weka.core.Attribute;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.text.DateFormatSymbols;
import java.util.*;

/**
 * @author Craig Feldman
 * Date Created: 2015-09-16
 *
 * Stores some data that is used by various aspects of the program.
 */
public class Data {

	public final static String MODEL_PATH = "/WEB-INF/input_files/bayes2.model";
	public final static String NAMES_PATH = "WEB-INF/input_files/names.txt";

	// Stores a set containing month names
	private static final Set monthSet;
	// Change here if you want localised month names
	private static String[] longMonthNames = new DateFormatSymbols().getMonths();
	private static String[] shortMonthNames = new DateFormatSymbols().getShortMonths();
	// Stores names from a dictionary of names
	private static Set<String> nameSet;
	// Dublin Core Fields
	private static List<String> classVal = new ArrayList<>(
			Arrays.asList(
					"Contributor",
					"Coverage",
					"Creator",
					"Date",
					"Description",
					"Format",
					"Identifier",
					"Language",
					"Publisher",
					"Relation",
					"Rights",
					"Source",
					"Subject",
					"Title",
					"Type")
	);
	private static Map<String, String[]> secondaryDCFields;
	private static int numDCFields = classVal.size();
	// The attributes/features
	private static List<Attribute> attributeList = new ArrayList<>(
			Arrays.asList(
				new Attribute("number of characters"),
				new Attribute("number of words"),
				new Attribute("number of months"),
				new Attribute("number of person names"),
				new Attribute("% of digits"),
				new Attribute("% of letters"),
				new Attribute("% of capitals"),
				new Attribute("@@class@@",classVal))
			);

	// Create a set containing the month names
	static {
		monthSet = new HashSet<>();
		for (String monthName : longMonthNames)
			monthSet.add(monthName.toLowerCase());
		for (String monthName : shortMonthNames)
			monthSet.add(monthName.toLowerCase());
	}

	// Links secondary DC fields to primary
	static {
		secondaryDCFields = new HashMap<>();

		secondaryDCFields.put("Contributor", new String[] {"Creator"});

		secondaryDCFields.put("coverage", new String[] {"Spatial", "Temporal"});

		secondaryDCFields.put("Creator", new String[] {});

		secondaryDCFields.put("Date", new String[] {
				"Available", "Created", "Date Accepted", "Date Copyrighted", "Date Submitted",
				"Issued", "Modified", "Valid" });

		secondaryDCFields.put("Description", new String[] {"Abstract", "Table Of Contents"});

		secondaryDCFields.put("Format", new String[] {"Extent", "Medium"});

		secondaryDCFields.put("Identifier", new String[] {"Bibliographic Citation"});

		secondaryDCFields.put("Language", new String[] {});

		secondaryDCFields.put("Publisher", new String[] {});

		secondaryDCFields.put("Relation", new String[] {
				"Conforms To", "Has Format", "Has Part", "Has Version", "Is FormatOf", "Is Part Of", "Is Referenced By",
				"Is Replaced By", "Is Required By", "Is Version Of", "References", "Replaces", "Requires", "Source" });

		secondaryDCFields.put("Rights", new String[] {"Access Rights ", "License"});

		secondaryDCFields.put("Source", new String[] {});

		secondaryDCFields.put("Subject", new String[] {});

		secondaryDCFields.put("Title", new String[] {"Alternative"});

		secondaryDCFields.put("Type", new String[] {});
	}

	/** @return a map that links a DC-primary to available DC-secondary */
	public static Map<String, String[]> getSecondaryDCFields() {
		return secondaryDCFields;
	}

	/** Loads a set that contains names */
	public static void createNamesSet (String namesFilePath) {
		nameSet = new HashSet<>();
		try (BufferedReader br = new BufferedReader(new FileReader(namesFilePath))) {
			String line;
			while ((line = br.readLine()) != null) {
				nameSet.add(line);
			}

		} catch (IOException e) {
			System.err.println(e.toString());
			System.err.println("Error accessing names database '" + namesFilePath + "'");
			System.err.println("Program will continue without names database. This is not recommended.");
			// e.printStackTrace();
		}
	}

	/** @return the number of DC fields */
	public static int getNumDCFields() {
		return numDCFields;
	}

	/** @return a set containing month names */
	public static Set getMonthSet() {
		return monthSet;
	}

	/** @return a set containing person names */
	public static Set<String> getNameSet() {
		return nameSet;
	}

	/** @return a list containing all the DC-primary fields */
	public static List<String> getDCFieldNames() {
		return classVal;
	}

	/** @return a list containing weka attributes */
	public static List<Attribute> getAttributeList() {
		return attributeList;
	}
}