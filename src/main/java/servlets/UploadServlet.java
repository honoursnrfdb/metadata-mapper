package servlets;

import com.google.gson.Gson;
import com.opencsv.CSVReader;
import mapping.ML;
import mapping.MetadataMap;
import services.Data;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.List;
import java.util.Map;

/**
 * @author Craig Feldman
 * Date created: 2015-07-21.
 */
@WebServlet(name = "metadata-mapping")
@MultipartConfig
public class UploadServlet extends HttpServlet {


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Part inputFilePart = request.getPart("file-upload"); // Retrieves <input type="file" name="file-upload">
		//Part mappingFilePart = request.getPart("mapping-upload");
		String inputFileName =  inputFilePart.getSubmittedFileName();
		//String mappingFileName = mappingFilePart.getSubmittedFileName();

		String mappingFile = request.getParameter("mapping-file");
		System.out.println("Mapping File: " + mappingFile);

		// prior mapping being used so we can skip ahead
		if (!mappingFile.isEmpty()) {
			request.getSession().setAttribute("hasPriorMapping", true);
			System.out.println("FORWARDED");
			request.getRequestDispatcher("/mapping2").forward(request, response);
			System.out.println("DONE FORWARDED");
			return;

		}


		char separator = request.getParameter("separators").equals("\\t") ? '\t' : request.getParameter("separators").charAt(0) ;

		String checkBoxValue = request.getParameter("has-header");
		boolean hasHeader = checkBoxValue != null;

		//InputStream fileInputStream = inputFilePart.getInputStream();


		String tDir = System.getProperty("java.io.tmpdir");

		try (InputStream fileInputStream = inputFilePart.getInputStream()){
			File tmp = new File(tDir, "TEMP.csv");
			System.out.println("location: " + tmp.getAbsolutePath().toString());

			Files.copy(fileInputStream, tmp.toPath(), StandardCopyOption.REPLACE_EXISTING);
		}

		// We need to reference the classifier and names files
		ServletContext context = getServletContext();
		InputStream modelFileInputStream = context.getResourceAsStream(Data.MODEL_PATH);
		// Set the file path for the names database
		Data.createNamesSet(context.getRealPath("") + Data.NAMES_PATH);

		// Set the file used by ML for predictions
		ML.setClassifierFile(modelFileInputStream);

		MetadataMap mapper = new MetadataMap();
		InputStream inputFile = new FileInputStream(tDir + "/TEMP.csv");
		CSVReader reader = new CSVReader(new InputStreamReader(inputFile), separator);
		mapper.parseCSV(reader, hasHeader);
		reader.close();
		mapper.convertScoresToPercentage();

		// Set the header attribute tha contains column headings
		String[] header = mapper.getHeader();

		List<List<Map.Entry<String, Integer>>> results = mapper.getSortedTracker();


		System.out.println("Input File Name: " + inputFileName);
		System.out.println("sep: " + separator + "|end");
		System.out.println("has header: " + checkBoxValue);
		System.out.println("Input File Name: " + inputFileName);
		System.out.println(context.getRealPath("") + Data.NAMES_PATH);
		mapper.debugTracker();
		for (int i = 0; i < header.length; i++) {
			List<Map.Entry<String, Integer>> list = results.get(i);
			System.out.println(list.toString());
		}

		request.getSession().setAttribute("mappingResults", results);
		request.getSession().setAttribute("numDCFields", Data.getNumDCFields());
		request.getSession().setAttribute("headerFields", header);
		// Some sample data for the fields (used by popover)
		request.getSession().setAttribute("sampleContent", mapper.getFormattedSampleContent());
		// the number of fields/columns in the data
		request.getSession().setAttribute("numFields", mapper.getNumFields());

		// Create a Json object for the secondary field map
		Gson gson = new Gson();
		String json = gson.toJson(Data.getSecondaryDCFields());
		request.getSession().setAttribute("secondaryDCFields", json);

		// Forward to JSP
		request.getRequestDispatcher("/Mapping.jsp").forward(request, response);
		System.out.println("post done");
	}
}
