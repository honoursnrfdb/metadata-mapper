package servlets;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;

/**
 * Created by Craig Feldman on 2015-09-21.
 *
 * Extracts the mapping and stores it in an array
 */
@WebServlet(name = "mapping-generator")
@MultipartConfig
public class MappingServlet extends HttpServlet {
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		System.out.println("Mapping post started");

		if (request.getSession().getAttribute("hasPriorMapping") != null) {
			System.out.println("RECEVED AND HASPRIOR MAPPING");
			return;
		}

		// Stores the primary DC field where each index corresponds to the column at that index
		String[] primaryDC = request.getParameterValues("DC-primary");
		// Stores the secondary DC field where each index corresponds to the column at that index
		String[] secondaryDC = request.getParameterValues("DC-secondary");

		int numFields =primaryDC.length;

		System.out.println(Arrays.toString(primaryDC));
		System.out.println(Arrays.toString(secondaryDC));

		/*
		String tDir = System.getProperty("java.io.tmpdir");
		InputStream inputFile = new FileInputStream(tDir + "/TEMP.csv");

		CSVReader reader = new CSVReader(new InputStreamReader(inputFile), ';');
		List<String[]> all = reader.readAll();
		for (String[]s : all)
			System.out.println(Arrays.toString(s));

		reader.close();
		 */

		String[] dcTitles = new String[numFields];
		for (int i = 0; i < numFields; ++i){
			// lowercase first letter
			dcTitles[i] = "dc." + Character.toLowerCase(primaryDC[i].charAt(0)) + primaryDC[i].substring(1);
			if (!secondaryDC[i].isEmpty()) {
				// Remove spaces and convert to camel case
				secondaryDC[i] = secondaryDC[i].replace(" ", "");
				dcTitles[i] += "." + Character.toLowerCase(secondaryDC[i].charAt(0)) + secondaryDC[i].substring(1);
			}
		}

		System.out.println(Arrays.toString(dcTitles));

		System.out.println("Mapping post finished");

	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		System.err.println("Mapping get started");
	}
}
